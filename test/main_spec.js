/*jslint browser: false, node: true*/
/*global expect*/
'use strict';

const frisby = require('frisby');
const config = require('./config.json');

console.log('API url: ' + config.url);


frisby.globalSetup({ // globalSetup is for ALL requests
    request: {
        strictSSL: false,
        baseUri: config.url
    }
});


frisby.create('Get an array of questionnaires')
    .get('/')
    .expectStatus(200)
    .expectJSONTypes([{id: String, title: String, description: String}])
    .toss();


frisby.create('Get nonexistent questionnaire')
    .get('/doesnt_exist')
    .expectStatus(404)
    .toss();


frisby.create('Get nonexistent questions')
    .get('/doesnt_exist/questions')
    .expectStatus(404)
    .toss();


frisby.create('Get "gulenko" questionnaire')
    .get('/gulenko')
    .expectStatus(200)
    .expectJSONTypes({})
    .toss();


frisby.create('Get all questions')
    .get('/gulenko/questions')
    .expectStatus(200)
    .expectJSONTypes({})
    .afterJSON(function (questions) {
        expect(Object.keys(questions).length > 0).toBe(true);
    })
    .toss();


frisby.create('Get one nonexistent question')
    .get('/gulenko/questions/-1')
    .expectStatus(404)
    .toss();


frisby.create('Get one question')
    .get('/gulenko/questions/1')
    .expectStatus(200)
    .afterJSON(function (question) {
        expect(Object.keys(question).length > 0).toBe(true);
    })
    .toss();



frisby.create('Get answers for nonexistent user')
    .get('/gulenko/samples/ImNotAUser')
    .expectStatus(404)
    .toss();


frisby.create('Get unanswered questions')
    .get('/gulenko/samples/testSample/unanswered')
    .expectStatus(200)
    .expectJSON({})
    .afterJSON(function (questions) {
        expect(Object.keys(questions).length).toBe(72);
    })
    .toss();


frisby.create('Get answered questions')
    .get('/gulenko/samples/testSample/answered')
    .expectStatus(200)
    .expectJSON({})
    .afterJSON(function (questions) {
        expect(Object.keys(questions).length).toBe(0);
    })
    .toss();


frisby.create('Update a sample')
    .post('/gulenko/samples/testSample')
    .expectStatus(404)
    .toss();


frisby.create('Update a sample with wrong question key')
    .post('/gulenko/samples/testSample/answers?FAKE=S')
    .expectStatus(400)
    .expectJSONTypes({error: String})
    .toss();


frisby.create('Update a sample with wrong answer key')
    .post('/gulenko/samples/testSample/answers?1=M')
    .expectStatus(400)
    .expectJSONTypes({error: String})
    .toss();


var sample = Math.random() * 1E6 | 0;

frisby.create('Answer one question')
    .post('/gulenko/samples/' + sample + '/answers?1=S')
    .expectStatus(200)
    .after(function () {

        var cnt = 0;

        frisby.create('Get unanswered questions')
            .get('/gulenko/samples/' + sample + '/unanswered')
            .expectStatus(200)
            .afterJSON(function (questions) {
                cnt = Object.keys(questions).length;
                expect(cnt).toBe(71);
            })
            .toss();

        frisby.create('Get one answer')
            .get('/gulenko/samples/' + sample + '/answers')
            .expectStatus(200)
            .afterJSON(function (answers) {
                expect(Object.keys(answers).length).toBe(1);
                expect(answers[1]).toBe('S');

                frisby.create('Get intermediate result')
                    .get('/gulenko/samples/' + sample + '/result')
                    .expectStatus(200)
                    .expectJSONTypes({error: String})
                    .toss();

                (function answer() {
                    frisby.create('Get rest of unanswered questions')
                        .get('/gulenko/samples/' + sample + '/unanswered')
                        .expectStatus(200)
                        .afterJSON(function (questions) {
                            var keys = Object.keys(questions);
                            if (keys.length) {
                                var key = keys[0];
                                var q = questions[key];
                                var a = Object.keys(q)[Math.random() * 2 | 0];

                                // wahchdog
                                cnt -= 1;
                                if (cnt < 0) {
                                    throw new Error('Number of questions exceeded maximum');
                                }

                                frisby.create('Answer it')
                                    .post('/gulenko/samples/' + sample + '/answers?' + key + '=' + a)
                                    .expectStatus(200)
                                    .after(answer)
                                    .toss();


                            } else {

                                frisby.create('Get final result')
                                    .get('/gulenko/samples/' + sample + '/result')
                                    .expectStatus(200)
                                    .expectJSONTypes({sorted: [{encoded: String, probability: Number}]})
                                    .afterJSON(function (result) {
                                        expect(Object.keys(result.sorted).length).toBe(16);
                                    })
                                    .toss();
                            }
                        })
                        .toss();
                }());
            })
            .toss();
    })
    .toss();

