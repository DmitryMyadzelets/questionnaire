# Questionnaires

## Implementation decisions
+ One question implies one or no answer
+ User must be encoded in JSON Web Token (JWT)
+ Questions and answers should be stored separately
+ A questionnaire shouldn't be changed. If you should - change version
+ Answers should keep a link to a correspondent questionnaire
+ Answers and questions are object, no arrays
+ An answer is a tuple (answerId, value)

## API

Get all questionnaires
> GET /questionnaires

Get a questionnaire
> GET /questionnaires/:qid

Update answers for a subset of questions for an encoded user
> POST /questionnaires/:qid/answers

Get answers for an encoded user
> GET /questionnaires/:qid/answers

Get unanswered questions for an encoded user
> GET /questionnaires/:qid/unanswered

Get answered questions for an encoded user
> GET /questionnaires/:qid/answered

Get questions\answers for an user (not implemented)
> GET /questionnaires/:qid/users/:uid/[answers, unanswered, answered]

Get computed result for an encoded user
> GET /questionnaires/:gid/result

## Testing

For tests you should have installed `jasmine node` and `frisby`, e.g.:

    sudo npm install -g jasmine-node
    sudo npm install -g frisby
