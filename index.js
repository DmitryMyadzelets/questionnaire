/*jslint browser: false*/
'use strict';

// External modules
const express = require('express');
// const bodyParser = require('body-parser');
// const cookies = require('cookie-parser');
// const jwt = require('jwt-simple');
// Internal modules
const model = require('./model');
const config = require('./.config.json');
const port = config.port;
// const jwtSecret = config.jwtSecret;

const app = express();

app.disable('x-powered-by'); // Remove 'X-Powered-By: Express' HTTP header
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({extended: true})); // to support URL-encoded bodies
// app.use(cookies());

app.listen(port, function () {
    console.log('Questionnaire microservice listening on port', port);
});


model.init(function (err) {
    if (err) {
        console.log(err, err.stack);
        return process.exit(1);
    }
});


// ============================================================================

// Debugging
app.all('*', function (req, ignore, next) {
    console.log(req.method + ' ' + req.protocol + '://' + req.hostname + req.originalUrl);
    next();
});


// Get all questionnaires
app.get('/', function (ignore, res, next) {
    model.questionnaires.all(function (err, questionnaires) {
        if (err) {
            return next(err);
        }
        res.json(questionnaires);
    });
});


// ----------------------------------------------------------------------------
// Questionnaire-dependent API

// Get a questionnaire by id
app.param('questionnaireId', function (req, res, next, id) {
    model.questionnaires.one(id, function (err, questionnaire) {
        if (err) {
            return next(err);
        }
        if (!questionnaire) {
            return res.sendStatus(404);
        }
        req.questionnaire = questionnaire;
        next();
    });
});


// The router for a questionnaire
var quest = new express.Router();
app.use('/:questionnaireId', quest);


// Return current questionnaire
quest.get('/', function (req, res) {
    res.json(req.questionnaire);
});


// Return all questions for current questionnaire
quest.get('/questions', function (req, res) {
    res.json(req.questionnaire.questions);
});


// Return one question
quest.get('/questions/:questionId', function (req, res) {
    var question = req.questionnaire.questions[req.params.questionId];
    if (!question) {
        return res.sendStatus(404);
    }
    res.json(question);
});


// ============================================================================
// Sample-dependent API

// Sets the sample id or a correspondent object
// Other methods must verify the type of the req.sample
quest.param('sampleId', function (req, ignore, next, id) {
    model.samples.one(req.questionnaire, id, function (err, sample) {
        if (err) {
            return next(err);
        }
        req.sample = sample || id;
        next();
    });
});


// The router for a sample
var sample = new express.Router();
quest.use('/samples/:sampleId', sample);


// Get sample
sample.get('/', function (req, res) {
    if (typeof req.sample !== 'object') {
        return res.sendStatus(404);
    }
    res.json(req.sample);
});


sample.route('/answers')
    // Get sample's answers
    .get(function (req, res) {
        if (typeof req.sample !== 'object') {
            return res.json({});
        }
        res.json(req.sample.answers);
    })
    // Update sample's answers
    .post(function (req, res, next) {
        // Create sample if it's a new one
        if (typeof req.sample !== 'object') {
            req.sample = model.samples.create(req.questionnaire, req.sample);
        }
        model.answers.update(req.questionnaire, req.sample, req.query, function (err, answers) {
            if (err) {
                return next(err);
            }
            if (!answers) {
                return res.sendStatus(404);
            }
            res.json(answers);
        });

    });


// Get unanswered questions
sample.get('/unanswered', function (req, res, next) {
    // No sample means no answers for any question
    if (typeof req.sample !== 'object') {
        return res.json(req.questionnaire.questions);
    }
    model.questions.unanswered(req.questionnaire, req.sample, function (err, questions) {
        if (err) {
            return next(err);
        }
        if (!questions) {
            return res.sendStatus(404);
        }
        res.json(questions);
    });
});


// Get answered questions
sample.get('/answered', function (req, res, next) {
    // No sample means no answers and answered questions
    if (typeof req.sample !== 'object') {
        return res.json({});
    }
    model.questions.answered(req.questionnaire, req.sample, function (err, questions) {
        if (err) {
            return next(err);
        }
        if (!questions) {
            return res.sendStatus(404);
        }
        res.json(questions);
    });
});



// Return result
sample.get('/result', function (req, res, next) {
    if (typeof req.sample !== 'object') {
        return res.sendStatus(404);
    }
    model.result(req.questionnaire, req.sample, function (err, result) {
        if (err) {
            return next(err);
        }
        if (!result) {
            return res.sendStatus(404);
        }
        res.json(result);
    });
});


// Process errors
app.use(function (err, ignore, res, next) {
    res.status(err.code || 500);
    res.json({error: err.message});
    next();
});
