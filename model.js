/*jslint browser: false*/
'use strict';


const db = {
    questionnaires: {},
    samples: []
};


// Load questionnaires
try {
    db.questionnaires.gulenko = require('./questionnaires/gulenko.ru.json');
    db.questionnaires.gulenko.id = 'gulenko';
} catch (e) {
    console.log(e);
}


const questionnaires = {
    all: function (callback) {
        var list = Object.keys(db.questionnaires).map(function (k) {
            return {id: k, title: db.questionnaires[k].title, description: db.questionnaires[k].description || ''};
        });
        callback(null, list);
    },
    one: function (id, callback) {
        callback(null, db.questionnaires[id]);
    }
};


const questions = {
    answered: function (questionnaire, sample, callback) {
        var result = Object.keys(sample.answers).reduce(function (result, key) {
            var question = questionnaire.questions[key];
            if (question) {
                result[key] = question;
            }
            return result;
        }, {});
        callback(null, result);
    },
    unanswered: function (questionnaire, sample, callback) {
        var result = Object.keys(questionnaire.questions).reduce(function (result, key) {
            if (!sample.answers[key]) {
                result[key] = questionnaire.questions[key];
            }
            return result;
        }, {});
        callback(null, result);
    }
};


const samples = {
    one: function (questionnaire, sampleId, callback) {
        var result = db.samples.filter(function (o) {
            return o.questionnaireId === questionnaire.id && o.id === sampleId;
        });
        callback(null, result.length && result[0]);
    },
    create: function (questionnaire, sampleId) {
        var sample = {
            questionnaireId: questionnaire.id,
            id: sampleId,
            answers: {}
        };
        db.samples.push(sample);
        return sample;
    }
};


const answers = {
    update: function (questionnaire, sample, answers, callback) {
        var badKeys = Object.keys(answers).reduce(function (o, key) {
            if (!questionnaire.questions[key]) {
                o[key] = answers[key];
            } else if (!questionnaire.questions[key][answers[key]]) {
                o[key] = answers[key];
            }
            return o;
        }, {});

        if (Object.keys(badKeys).length) {
            var err = new Error('No questions found for the answers: ' + JSON.stringify(badKeys));
            err.code = 400;
            return callback(err);
        }
        // TODO validate the answers against the questions
        callback(null, Object.assign(sample.answers, answers));
    }
};


var result = function (questionnaire, sample, callback) {
    var answerKeys = Object.keys(sample.answers);
    // Require answers for all questions
    var len = answerKeys.length;
    var max = Object.keys(questionnaire.questions).length;
    if (len !== max) {
        return callback(null, {
            error: 'Expected ' + max + 'answers but got ' + len
        });
    }
    // Count keys of answers
    var choice = answerKeys.reduce(function (o, c, ignore, keys) {
        o[keys[c]] |= 0;
        o[keys[c]] += 1;
        return o;
    }, {});
    // Normalize in ranage [0..1]
    Object.keys(choice).reduce(function (o, c) {
        o[c] /= 18;
        return o;
    }, choice);
    // Get encoded types
    var encoded = Object.keys(questionnaire.types.default).reduce(function (o, t) {
        o[t] = t.split('').reduce(function (sum, c) {
            sum *= choice[c] || 0;
            return sum;
        }, 1);
        return o;
    }, {});
    // Get sorted probabilities of encoded types
    var pp = Object.keys(encoded).sort(function (a, b) {
        return encoded[b] - encoded[a];
    });
    // Get readable result
    var sorted = pp.map(function (type) {
        return {
            encoded: type,
            probability: encoded[type],
            default: questionnaire.types.default[type],
            aushra: questionnaire.types.aushra[type],
            gulenko: questionnaire.types.gulenko[type],
            mbti: questionnaire.types.mbti[type]
        };
    });
    //
    callback(null, {
        sorted: sorted
    });
};


function init(callback) {
    var err = null;
    callback(err);
}


exports.questionnaires = questionnaires;
exports.questions = questions;
exports.answers = answers;
exports.samples = samples;
exports.result = result;
exports.init = init;
